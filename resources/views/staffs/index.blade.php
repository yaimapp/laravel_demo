@extends('layouts.app')

@section('content')
    <h1>担当者一覧</h1>
    <div class="panel-body">
        @include('common.errors')
        
        <form action="/staffs" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            
            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">名前</label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-3">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> 追加する
                    </button>
                </div>
            </div>
        </form>
    </div>
    
    @if (count($staffs))
        <div class="panel panel-default">
            <div class="panel-body">
                <table class="table table-striped table-condensed table-hover" id="staffs">
                    <thead>
                        <th class="staff-name">名前</th>
                        <th class="staff-work">操作</th>
                    </thead>
                    <tbody>
                        @foreach ($staffs as $staff)
                            <tr>
                                <td class="table-text">
                                    <div>{{ $staff->name }}</div>
                                </td>
                                <td>
                                    <form action="/staffs/{{ $staff->id }}" method="POST" class="form-inline">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <div class="form-group">
                                            <a href="/staffs/{{ $staff->id }}/edit"
                                               class="btn btn-default btn-sm" title="編集">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn-destroy btn btn-sm" title="削除">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@stop

@section('script')
$(function(){
    $(".btn-destroy").click(function(){
        if(confirm("本当に削除しますか？")) {
        } else {
            return false;
        }
    });
});
@stop