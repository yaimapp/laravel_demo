@extends('layouts.app')

@section('content')
    <h1>編集</h1>

    <div class="row">
        <div class="col-sm-12">
            <a href="/tags" class="btn btn-primary" style="margin:20px">一覧に戻る</a>
        </div>
    </div>
    <div class="panel-body">

        @include('common.errors')

        <form action='/tags/{{$tag->id}}' method="POST" class="form-horizontal">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}

            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">タグ名</label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="name" value="{{$tag->name}}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-3">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-save"></i> 保存する
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop
