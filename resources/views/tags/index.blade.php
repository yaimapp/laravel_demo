@extends('layouts.app')

@section('content')
    <h1>タグ一覧</h1>
    <div class="panel-body">

        @include('common.errors')

        <form action='/tags' method="POST" class="form-horizontal">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">タグ名</label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-3">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-plus"></i> 追加する
                    </button>
                </div>
            </div>
        </form>
    </div>

    @if (count($tags))
        <div class="panel panel-default">
            <div class="panel-body">
                <table class="table table-striped table-condensed table-hover construction-table " id="constructions">
                    <thead>
                        <th class="tag-name">名前</th>
                        <th class="tag-work">操作</th>
                    </thead>
                    <tbody>
                        @foreach ($tags as $tag)
                            <tr>
                                <td class="table-text">
                                    <div>{{ $tag->name }}</div>
                                </td>
                                <td>
                                    <form method="post" action="/tags/{{ $tag->id }}" class="form-inline">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <div class="form-group">
                                            <a href="/tags/{{$tag->id}}/edit"
                                              class="btn btn-default btn-sm"
                                              title="編集">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn-destroy btn btn-sm"
                                                title="削除">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@stop

@section('script')
$(function(){
    $(".btn-destroy").click(function(){
        if(confirm("本当に削除しますか？")){

        } else {
            return false;
        }
    });
});
@stop
