@extends('layouts.app')

@section('content')
    <h1>実績一覧</h1>
    
    <div class="row">
        <div class="col-sm-12">
            <a href="/constructions/create" class="btn btn-primary" style="margin: 20px;">新規登録</a>
        </div>
    </div>
    
    @if (count($constructions) > 0)
        <div class="panel panel-default">
            <div class="panel-body">
                <table class="table table-striped table-condensed table-hover construction-table " id="constructions">
                    <thead>
                        <th>年度</th>
                        <th>期間</th>
                        <th>工事名</th>
                        <th>担当者</th>
                        <th>担当者2</th>
                        <th>担当者3</th>
                        <th>ポイント</th>
                        <th>タグ</th>
                        <th>操作</th>
                    </thead>
                    <tbody>
                        @foreach ($constructions as $const)
                            <tr>
                                <td class="table-text">
                                    <div>{{ $const->year }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $const->period }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $const->const_name }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $staff[$const->staff] }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $staff[$const->staff2] }}</div>
                                </td>
                                <td class="table-text">
                                    <div>{{ $staff[$const->staff3] }}</div>
                                </td>
                                <td class="numeric">
                                    <div>{{ $const->point }}</div>
                                </td>
                                <td class="table-text">
                                    <div>
                                      @unless ($const->tags->isEmpty())
                                      {!! implode('<br>', $const->tags->pluck('name')->toArray()) !!}
                                      @endunless
                                    </div>
                                </td>
                                <td>
                                    <form method="post" action="/constructions/{{ $const->id }}" class="form-inline">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <div class="form-group">
                                            <a href="/constructions/{{$const->id}}/edit"
                                              class="btn btn-default btn-sm"
                                              title="編集">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn-destroy btn btn-sm"
                                                title="削除">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="paging"></div>
            </div>
            
        </div>
    @endif
@stop

@section('script')
$(function(){
    $(".btn-destroy").click(function(){
        if(confirm("本当に削除しますか？")){
        
        } else {
            return false;
        }
    });
    $("#constructions").datatable({
        pageSize: 20,
        sort: [true, true, true, true, true, true, false],
        filters: [true, true, 'select', 'select', 'select' ,true, false],
        filterText: '絞込'
    });
});
@stop