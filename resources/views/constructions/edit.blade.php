@extends('layouts.app')

@section('content')
    <h1>編集</h1>
    
    <div class="row">
        <div class="col-sm-12">
            <a href="/constructions" class="btn btn-primary" style="margin:20px">一覧に戻る</a>
        </div>
    </div>
    
    
    <div class="panel-body">
        
        @include('common.errors')
        
        <form action='/constructions/{{$construction->id}}' method="POST" class="form-horizontal">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            
            <div class="form-group">
                <label for="years" class="col-sm-3 control-label">年度</label>
                <div class="col-sm-6">
                    <input type="text" name="years" id="years" class="form-control" value="{{ $construction->years }}">
                </div>
            </div>

            <div class="form-group">
                <label for="period" class="col-sm-3 control-label">期間</label>
                <div class="col-sm-6">
                    <input type="text" name="period" id="period" class="form-control" value="{{ $construction->period }}">
                </div>
            </div>
            
            <div class="form-group">
                <label for="const-name" class="col-sm-3 control-label">工事</label>
                <div class="col-sm-6">
                    <input type="text" name="const_name" id="const-name" class="form-control"
                        value="{{ $construction->const_name }}">
                </div>
            </div>
            
            <div class="form-group">
                <label for="staff" class="col-sm-3 control-label">担当者</label>
                <div class="col-sm-6">
                    <select class="form-control" name="staff">
                        @foreach($staff as $id => $value)
                            @if ($id == $construction->staff)
                              <option value="{{ $id }}" selected>{{ $value }}</option>
                            @else
                              <option value="{{ $id }}">{{ $value }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            
            <div class="form-group">
                <label for="staff2" class="col-sm-3 control-label">担当者2</label>
                <div class="col-sm-6">
                    <select class="form-control" name="staff2">
                        @foreach($staff as $id => $value)
                            @if ($id == $construction->staff2)
                              <option value="{{ $id }}" selected>{{ $value }}</option>
                            @else
                              <option value="{{ $id }}">{{ $value }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            
            <div class="form-group">
                <label for="staff3" class="col-sm-3 control-label">担当者3</label>
                <div class="col-sm-6">
                    <select class="form-control" name="staff3">
                        @foreach($staff as $id => $value)
                            @if ($id == $construction->staff3)
                              <option value="{{ $id }}" selected>{{ $value }}</option>
                            @else
                              <option value="{{ $id }}">{{ $value }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            
            <div class="form-group">
                <label for="point" class="col-sm-3 control-label">ポイント</label>
                <div class="col-sm-6">
                    <input type="number" name="point" id="point" value="{{$construction->point}}" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label for="tags[]" class="col-sm-3 control-label">タグ</label>
                <div class="col-sm-6">
                    <select class="form-control" name="tags[]" multiple="true" id="tags">
                        @foreach($tags as $id => $value)
                            @if (in_array($id, $construction->tags->pluck('id')->toArray()))
                                <option value="{{ $id }}" selected>{{ $value }}</option>
                            @else
                                <option value="{{ $id }}" >{{ $value }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-save"></i> 保存する
                    </button>
                </div>
            </div>
        </form>
    </div>

@stop

@section('script')
$(function() {
   $("#datepicker").datepicker({
       format: 'yy-mm-dd',
       language: 'ja'
   });
});
@stop