<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Construction;
use App\Repositories\ConstructionRepository;
use App\Staff;
use App\Tag;

class ConstructionController extends Controller
{
    protected $constructions;
    protected $staff;
    protected $tags;
    
    public function __construct(ConstructionRepository $constructions)
    {
        $this->middleware('auth');
        $this->constructions = $constructions;
        $this->staff = Staff::pluck('name', 'id')->toArray();
        $this->tags = Tag::pluck('name', 'id')->toArray();
    }
    
    public function index(Request $request)
    {
        $constructions = $this->constructions->all();
        
        return view('constructions.index', array(
          'constructions' => $constructions,
          'staff' => $this->staff,
        ));
    }
    
    public function create()
    {
        return view('constructions.create', array(
          'staff' => $this->staff,
          'tags' => $this->tags,
        ));
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'years'      => 'required',
            'const_name' => 'required',
            'point'      => 'required',
        ]);
        
        $construction = new Construction;
        $construction->years    = $request->years;
        $construction->period   = $request->period;
        $construction->const_name = $request->const_name;
        $construction->staff    = $request->staff;
        $construction->staff2   = $request->staff2;
        $construction->staff3   = $request->staff3;
        $construction->point      = $request->point;
        $construction->save();
        
        $tagIds = $request->input('tags');
        if (count($tagIds) > 0) {
            $construction->tags()->attach($tagIds);
        }
        
        return redirect('constructions');
    }
    
    public function edit($id)
    {
        $construction = Construction::find($id);
        
        return view('constructions.edit', array(
          'construction' => $construction,
          'staff' => $this->staff,
          'tags'  => $this->tags,
        ));
    }
    
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'years'      => 'required',
            'const_name' => 'required',
            'staff'      => 'required',
            'point'      => 'required',
        ]);
        
        $construction = Construction::find($id);
        $construction->years      = $request->years;
        $construction->period     = $request->period;
        $construction->const_name = $request->const_name;
        $construction->staff      = $request->staff;
        $construction->staff2     = $request->staff2;
        $construction->staff3     = $request->staff3;
        $construction->point      = $request->point;
        $construction->save();
        
        $tagIds = $request->input('tags');
        if (count($tagIds) > 0) {
          $construction->tags()->sync($tagIds);
        } else {
          $construction->tags()->detach();
        }

        return redirect('constructions');
    }
    
    public function destroy(Request $request, $id)
    {
        $construction = Construction::find($id);
        $construction->delete();
        
        return redirect('constructions');
    }
}
