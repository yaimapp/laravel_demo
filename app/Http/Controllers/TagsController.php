<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    public function index()
    {
        $tags = Tag::all();
        return view('tags.index', compact('tags'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        
        $tag = new Tag;
        $tag->name = $request->name;
        $tag->save();

        return redirect('tags');
    }

    public function edit($id)
    {
        $tag = Tag::find($id);
        return view('tags.edit', array(
          'tag' => $tag,
        ));
    }
    
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);
        
        $tag = Tag::find($id);
        $tag->name = $request->name;
        $tag->save();

        return redirect('tags');
    }
    
    public function destroy(Request $request, $id)
    {
        $tag = Tag::find($id);
        $tag->delete();
        
        return redirect('tags');
    }
}
