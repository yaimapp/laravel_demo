<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'name',
    ];

    public function constructions() {
        return $this->belongsToMany('App\Construction');
    }
}
