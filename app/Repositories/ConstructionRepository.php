<?php

namespace App\Repositories;

use App\Construction;

class ConstructionRepository
{
    public function all()
    {
        return Construction::orderBy('const_date', 'desc')
            ->get();
    }
}