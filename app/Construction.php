<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Construction extends Model
{
    protected $fillable = [
        'years',
        'period',
        'const_name',
        'staff',
        'staff2',
        'staff3',
        'point'
    ];
    
    public function tags()
    {
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }
    
    public function getTagLIstAttribute()
    {
        return $this->tags->pluck('id')->toArray();
    }
}
