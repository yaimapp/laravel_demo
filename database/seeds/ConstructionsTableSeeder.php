<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;
use Carbon\Carbon;
use App\Construction;

class ConstructionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Construction::truncate();
        
        $faker = Faker::create('ja_JP');
        
        for($i = 0; $i < 50; $i++)
        {
            $const = new Construction();
            
            $const->const_date = $faker->date($format='Y-m-d',$max='now', $min='');
            $const->const_name = $faker->city;
            $const->const_type = sprintf('type%02d', mt_rand(1, 4));
            $const->staff = sprintf('staff%02d', mt_rand(1, 10));
            $const->client = sprintf('client%02d', mt_rand(1, 8));
            $const->point = $faker->randomNumber(3);
            
            $const->save();
        }
    }
}
