<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ConstructionController@index'); 

Auth::routes();

Route::resource('constructions', 'ConstructionController',
    ['except' => ['show']]);

Route::resource('staffs', 'StaffController',
    ['except' => ['show', 'create']]);
    
Route::resource('tags', 'TagsController',
    ['except' => ['show', 'create']]);